package com.example.demo.controller;

import com.example.demo.Modelo.Movie;
import com.example.demo.service.MovieService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/movies")
public class MovieController {
    @Autowired
    private MovieService movieService;

    @GetMapping
    public List<Movie> getMovies() {
    return movieService.getAllMovies();
    }

    @GetMapping("/{id}")
    public Movie getMovie(@PathVariable Long id) {
        return movieService.getMovieById(id);
    }
    @PostMapping
    public Movie addMovie(@RequestBody Movie movie) {
        Movie n = new Movie();
        n.setMovie_name("");
        n.setUrl("");
        return movieService.addMovie(movie);
    }
    @PutMapping("/{id}")
    public Movie updateMovie(@PathVariable Long id, @RequestBody Movie
            movie) {
        return movieService.updateMovie(id, movie);
    }
    @DeleteMapping("/{id}")
    public void deleteMovie(@PathVariable Long id) {
        movieService.deleteMovie(id);
    }


}
